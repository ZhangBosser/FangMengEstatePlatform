<?php

return array (
  0 => 
  array (
    'name' => 'mode',
    'title' => '模式',
    'type' => 'radio',
    'content' => 
    array (
      'fixed' => '固定',
      'random' => '每次随机',
      'daily' => '每日切换',
    ),
    'value' => 'daily',
    'rule' => 'required',
    'msg' => '',
    'tip' => '',
    'ok' => '',
    'extend' => '',
  ),
  1 => 
  array (
    'name' => 'image',
    'title' => '固定背景图',
    'type' => 'image',
    'content' => 
    array (
    ),
    'value' => '/uploads/20190622/8f28512e4d3d7fbdd65b0960d11c5b02.jpg',
    'rule' => 'required',
    'msg' => '',
    'tip' => '',
    'ok' => '',
    'extend' => '',
  ),
);
