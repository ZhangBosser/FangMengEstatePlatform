<?php

namespace app\common\model;

use think\Cache;
use think\Model;

/**
 * 房屋类型，新房二手房都使用
 */
class HouseTypes extends Model
{
    // 表名,不含前缀
    protected $name = 'newhouse_building_types';
}