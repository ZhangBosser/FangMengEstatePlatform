<?php

namespace app\common\model;

use think\Cache;
use think\Model;

/**
 * 二手房源
 */
class SecondBuildings extends Model
{
    // 表名,不含前缀
    protected $name = 'second_buildings';
    // 定义时间戳字段名
    // protected $createTime = 'add_time';
    // protected $updateTime = 'update_time';
}