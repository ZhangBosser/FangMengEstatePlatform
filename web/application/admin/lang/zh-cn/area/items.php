<?php

return [
    'Areaid'   => '地区Id',
    'Areacode' => '地区编码',
    'Areaname' => '地区名',
    'Level'    => '地区级别（1:省份province,2:市city,3:区县district,4:街道street）',
    'Citycode' => '城市编码',
    'Center'   => '城市中心点（即：经纬度坐标）',
    'Parentid' => '地区父节点'
];
