<?php

return [
    'Id'                  => '跟进编号',
    'Item_id'             => '二手房编号',
    'Create_userid'       => '跟进人',
    'Create_time'         => '跟进时间',
    'State'               => '房源状态',
    'Comment'             => '跟进内容',
    'Seconditems.address' => '地址',
    'Admin.nickname'      => '昵称'
];
