<?php

return [
    'Cratetime'                         => '报备时间',
    'Report_item_id'                    => '报备id',
    'Alias_name'                        => '共有产权人姓名',
    'Alias_tel'                         => '共有产权人电话',
    'Create_userid'                     => '操作人',
    'Relationship'                      => '客户关系',
    'Newhousereportitems.customer_id'   => '客户id',
    'Newhousereportitems.building_id'   => '楼盘id',
    'Newhousereportitems.createtime'    => '登记时间',
    'Newhousereportitems.verify_friend' => '老带新审核',
    'Newhousereportitems.create_userid' => '登记人',
    'Newhousereportitems.prepare_visit' => '预计看房时间',
    'Newhousereportitems.updatetime'    => '更新时间',
    'Newhousereportitems.update_userid' => '更新人',
    'Newhousereportitems.contents'      => '报备说明',
    'Newhousereportitems.verify_userid' => '审核人',
    'Newhousereportitems.verify_mem'    => '审核说明',
    'Newhousereportitems.verify_time'   => '审核时间',
    'Newhousereportitems.verify_state'  => '审核状态'
];
