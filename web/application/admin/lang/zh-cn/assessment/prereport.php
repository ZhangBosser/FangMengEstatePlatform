<?php

return [
    'Item_id'            => '标的编号',
    'Prereport_no'       => '预评编号',
    'Create_userid'      => '操作人',
    'Create_ip'          => '操作IP',
    'Contents'           => '内容',
    'Version'            => '版本',
    'Admin.id'           => 'ID',
    'Admin.pid'          => '父组织',
    'Admin.username'     => '用户名',
    'Admin.nickname'     => '昵称',
    'Admin.password'     => '密码',
    'Admin.salt'         => '密码盐',
    'Admin.avatar'       => '头像',
    'Admin.email'        => '电子邮箱',
    'Admin.loginfailure' => '失败次数',
    'Admin.logintime'    => '登录时间',
    'Admin.createtime'   => '创建时间',
    'Admin.updatetime'   => '更新时间',
    'Admin.token'        => 'Session标识',
    'Admin.status'       => '状态',
    'Admin.cop_id'       => '所属公司'
];
