<?php

return [
    'id'                => '编号',
    'Authcops.cop_name' => '企业名称',
    'Admin.nickname'    => '昵称',
    'department_id'     => '售楼部',
    'description'       => '说明',
    'ip'                => 'IP地址',
    'brand'             => '品牌'
];
