<?php

return [
    'Id'=>'编号',
    'Building_id'                           => '楼盘编号',
    'Createtime'                            => '登记时间',
    'Create_userid'                         => '登记人',
    'Lasttime'                              => '最后时间',
    'Last_userid'                           => '最后修改',
    'Path'                                  => '图片路径',
    'Typeid'                                => '图片类别',
    'Mem'                                   => '说明',
    'Newhousebuildingitems.building_name'   => '楼盘名称',
    'Newhousebuildingpicturetype.type_name' => '类型',
    'Newhousebuildingpicturetype.sort'      => '排序'
];
