<?php

return [
    'Id'                => '售楼部id',
    'Name'              => '售楼部名称',
    'Cop_id'            => '所属公司',
    'Address'           => '地址',
    'State'             => 'true有效 false无效',
    'Create_time'       => '录入时间',
    'Create_ip'         => '录入ip',
    'Create_userid'     => '录入人员',
    'Update_time'       => '更新时间',
    'Update_ip'         => '更新ip',
    'Update_userid'     => '更新人员',
    'Authcops.cop_name' => '企业名称',
    'Admin.nickname'    => '昵称'
];
