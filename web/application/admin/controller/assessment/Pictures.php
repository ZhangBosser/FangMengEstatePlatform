<?php

namespace app\admin\controller\assessment;

use app\common\controller\Backend;

/**
 * 楼盘相册
 *
 * @icon fa fa-circle-o
 */
class Pictures extends Backend
{
    
    /**
     * Pictures模型对象
     * @var \app\admin\model\assessment\Pictures
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\assessment\Pictures;

    }
    
    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    

    /**
     * 查看
     */
    public function index()
    {
        //当前是否为关联查询
        $this->relationSearch = true;
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax())
        {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField'))
            {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model
                    ->with(['assessmentitems','assessmentpicturetype'])
                    ->where($where)
                    ->order($sort, $order)
                    ->count();

            $list = $this->model
                    ->with(['assessmentitems','assessmentpicturetype'])
                    ->where($where)
                    ->order($sort, $order)
                    ->limit($offset, $limit)
                    ->select();

            foreach ($list as $row) {
                
                $row->getRelation('assessmentitems')->visible(['type_name']);
            }
            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }
    public function sync($item_id,$params){

        $db_picture_list = \app\admin\model\assessment\pictures::all(['item_id'=>$item_id]);
        foreach($params as $key=>$value){
            $array = explode(',',$value);
            //数据库里还没有的先新增
            foreach($array as $check=>$url){
                if(empty($value))continue;
                if($this->isInDb($key,$url,$db_picture_list)){

                }else{
                    $model=new \app\admin\model\assessment\Pictures;
                    $model->item_id = $item_id;
                    $model->create_userid = $this->auth->id;
                    $model->last_userid=$this->auth->id;
                    $model->path = $url;
                    $model->typeid = str_replace('path-','',$key);
                    $model->save();
                }
            }
        }
        
        // 数据库里的，如果不使用了删除
        foreach($db_picture_list as $key=>$value){
            if($this->isInParam("path-$value->typeid",$value->path,$params)){

            }else{
                $value->delete();
            }
        }
    }
    private function isInDb($path_typeid,$url,$db_picture_list){
        $has=false;
        foreach($db_picture_list as $key=>$record){
            if("path-$record->typeid" == $path_typeid && $url==$record->path){
                $has=true;
                break;
            }
        }
        return $has;
    }
    private function isInParam($path_type_id,$url,$params){
        $url_array = explode(',',$params[$path_type_id]);
        return in_array($url,$url_array);
    }
}
