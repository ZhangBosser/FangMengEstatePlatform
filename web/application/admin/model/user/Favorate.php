<?php

namespace app\admin\model\user;

use think\Model;


class Favorate extends Model
{

    

    

    // 表名
    protected $name = 'user_favorate';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'target_text'
    ];
    

    
    public function getTargetList()
    {
        return ['二手房出售房源' => __('二手房出售房源'), '二手房出租房源' => __('二手房出租房源'), '新房出售房源' => __('新房出售房源'), '新房出租房源' => __('新房出租房源'), '新房楼盘' => __('新房楼盘')];
    }


    public function getTargetTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['target']) ? $data['target'] : '');
        $list = $this->getTargetList();
        return isset($list[$value]) ? $list[$value] : '';
    }




    public function user()
    {
        return $this->belongsTo('app\admin\model\User', 'user_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
