<?php

namespace app\admin\model\second;

use think\Model;

class Items extends Model
{

    

    //数据库
    protected $connection = 'database';
    // 表名
    protected $name = 'second_items';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'add_time_text'
    ];
    

    



    public function getAddTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['add_time']) ? $data['add_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    protected function setAddTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }


    public function seconddecoration()
    {
        return $this->belongsTo('app\admin\model\second\Decoration', 'decoration_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }


    public function newhousebuildingtypes()
    {
        return $this->belongsTo('app\admin\model\newhouse\building\Types', 'type_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
    public function newhousebuilding(){
        return $this->belongsTo('app\admin\model\second\Buildings','building_id','id',[],'LEFT')->setEagerlyType(0);
    }


    public function authcops()
    {
        return $this->belongsTo('app\admin\model\auth\Cops', 'add_cop_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }


    public function admin()
    {
        return $this->belongsTo('app\admin\model\Admin', 'add_userid', 'id', [], 'LEFT')->setEagerlyType(0);
    }
    public function updateadmin()
    {
        return $this->belongsTo('app\admin\model\Admin', 'update_userid', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
