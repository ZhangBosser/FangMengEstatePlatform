<?php

namespace app\admin\model\newhouse\customer;

use think\Model;


class Follow extends Model
{

    

    //数据库
    protected $connection = 'database';
    // 表名
    protected $name = 'newhouse_customer_follow';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'state_text'
    ];
    

    
    public function getStateList()
    {
        return ['不确定'=>'不确定','无效' => __('无效'), '有效'=>'有效', '观望' => __('观望'),'带看' => __('带看'), '已购' => __('已购')];
    }


    public function getStateTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['state']) ? $data['state'] : '');
        $list = $this->getStateList();
        return isset($list[$value]) ? $list[$value] : '';
    }




    public function newhousecustomeritems()
    {
        return $this->belongsTo('app\admin\model\newhouse\customer\Items', 'customer_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
   
    public function newhouseadmin()
    {
        return $this->belongsTo('app\admin\model\Admin', 'create_userid', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
