<?php
namespace app\admin\model\assessment;

use think\Model;


class Appraisers extends Model
{

    

    //数据库
    protected $connection = 'database';
    // 表名
    protected $name = 'assessment_appraisers';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'lasttime_text'
    ];
    

    



    public function getLasttimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['lasttime']) ? $data['lasttime'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    protected function setLasttimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }


    public function authcops()
    {
        return $this->belongsTo('app\admin\model\auth\Cops', 'cop_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
