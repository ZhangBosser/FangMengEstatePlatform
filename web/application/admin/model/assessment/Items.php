<?php

namespace app\admin\model\assessment;

use think\Model;


class Items extends Model
{

    

    //数据库
    protected $connection = 'database';
    // 表名
    protected $name = 'assessment_items';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'create_time_text',
        'prereport_time_text',
        'prereport_verify_time_text',
        'prereport_state_text'
    ];
    public function getCreateTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['create_time']) ? $data['create_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getPrereportTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['prereport_time']) ? $data['prereport_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getPrereportVerifyTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['prereport_verify_time']) ? $data['prereport_verify_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    protected function setCreateTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    protected function setPrereportTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    protected function setPrereportVerifyTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    public function getPrereportStateList()
    {
        return ['0'=>'未预评','1'=>'已预评', '2'=>'审核通过', '3'=>'审核拒绝'];
    }

    public function getPrereportStateText()
    {
        $array = $this->getPrereportStateList();
        return $array[$this->prereport_state];
    }
    public function getPrereportStateTextAttr($value,$data)
    {
        $value = $value ? $value : (isset($data['prereport_state']) ? $data['prereport_state'] : '');
        $list = $this->getPrereportStateList();
        return isset($list[$value]) ? $list[$value] : '';
    }
    public function prereportappraisername(){
        return $this->belongsTo('app\admin\model\assessment\Appraisers', 'prereport_appraiser_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }

    public function houselandright()
    {
        return $this->belongsTo('app\admin\model\house\Landright', 'landright', 'id', [], 'LEFT')->setEagerlyType(0);
    }


    public function housestructure()
    {
        return $this->belongsTo('app\admin\model\house\Structure', 'structure', 'id', [], 'LEFT')->setEagerlyType(0);
    }


    public function houseplanpurpose()
    {
        return $this->belongsTo('app\admin\model\house\Planpurpose', 'planpurpose', 'id', [], 'LEFT')->setEagerlyType(0);
    }
    public function admin()
    {
        return $this->belongsTo('app\admin\model\Admin', 'create_userid', 'id', [], 'LEFT')->setEagerlyType(0);
    }
    public function authcops()
    {
        return $this->belongsTo('app\admin\model\auth\Cops', 'cop_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
    public function prereportadmin(){
        return $this->belongsTo('app\admin\model\Admin', 'prereport_userid', 'id', ['username'], 'LEFT')->setEagerlyType(0);
    }
}
