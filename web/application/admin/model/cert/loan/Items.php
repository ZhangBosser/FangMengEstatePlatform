<?php

namespace app\admin\model\cert\loan;

use think\Model;


class Items extends Model
{

    

    

    // 表名
    protected $name = 'cert_loan_items';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];
    

    







    public function certloanstate()
    {
        return $this->belongsTo('app\admin\model\cert\loan\State', 'state', 'state_id', [], 'LEFT')->setEagerlyType(0);
    }


    public function certloanbank()
    {
        return $this->belongsTo('app\admin\model\cert\loan\Bank', 'sign_bank', 'bank_id', [], 'LEFT')->setEagerlyType(0);
    }


    public function admin()
    {
        return $this->belongsTo('app\admin\model\Admin', 'create_userid', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
