<?php

namespace app\index\controller;

use app\common\controller\Frontend;

class Newhouse extends Frontend
{

    protected $noNeedLogin = '';
    protected $noNeedRight = '*';
    // protected $layout = 'default';

   
    public function customers(){
           //设置过滤方法
           $this->request->filter(['strip_tags']);
           
               //如果发送的来源是Selectpage，则转发到Selectpage
               if ($this->request->request('keyField')) {
                   return $this->selectpage();
               }
               $where='1=1';
               //list($where, $sort, $order, $offset, $limit) = $this->buildparams();
               $model = new \app\admin\model\newhouse\customer\Items;
               $total = $model
                   ->where($where)
                //    ->order($sort, $order)
                   ->count();
   
               $list = $model
                   ->where($where)
                //    ->order($sort, $order)
                //    ->limit($offset, $limit)
                   ->select();
   
               $list = collection($list)->toArray();
               $result = array("total" => $total, "rows" => $list);
   
               return json($result);
       
    }
}
