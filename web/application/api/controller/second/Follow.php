<?php

namespace app\api\controller\second;
use fast\Random;
use app\common\controller\Api;

/**
 * 二手房跟进接口
 */
class Follow extends Api
{

    // 无需登录的接口,*表示全部
    protected $noNeedLogin = [];
    // 无需鉴权的接口,*表示全部
    protected $noNeedRight = [];
    /**
     * Follow模型对象
     * @var \app\admin\model\second\Follow
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\common\model\SecondFollow;

    }
    /**
     * @ApiTitle (新增跟进房源)
     * @ApiSummary (二手房使用)
     * @ApiMethod (Post)
     * @ApiRoute (/api/second/follow/add)
     * @ApiParams   (name="row[item_id]", type="string",  required=fale,  description="房源编号")
     * @ApiParams   (name="row[state]", type="integer", required=false, description="状态")
     * @ApiParams   (name="row[comment]", type="string",  required=false, description="跟进内容")

     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                
                $house_id = $params['item_id'];
                $model =  \app\common\model\SecondItems::get($house_id);
                if(empty($model)){
                    $this->error('房源不存在');
                }

                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
                        $this->model->validateFailException(true)->validate($validate);
                    }
                    $params['create_userid'] = $this->auth->id;
                    $result = $this->model->allowField(true)->save($params);

                    $second_item_model->save();
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }
    /**
     * @ApiTitle (获取跟进房源列表)
     * @ApiSummary (二手房使用)
     * @ApiMethod (Get)
     * @ApiRoute (/api/second/follow/lists)
     * @ApiParams   (name="item_id",type="string",required=fale,description="房源编号")
     * @ApiParams   (name="offset", type="integer", required=false, description="分页偏移值")
     * @ApiParams   (name="limit", type="integer", required=false, description="分页大小，默认15，最大50")
     * @ApiParams   (name="sort", type="string", required=false, description="排序字段")
     * @ApiParams   (name="order", type="string", required=false, description="排序顺序,asc或desc")

     */
    public function lists($item_id){
        //当前是否为关联查询
        $this->relationSearch = false;
        //设置过滤方法
        $this->request->filter(['strip_tags']);

        list($where, $sort, $order, $offset, $limit) = $this->buildparams("");

        if(empty($item_id)){
            $where1='1=1';
        }else{
            $where1='item_id=' . $item_id;
        }
        $total = $this->model
            ->with('seconditem')
            ->where($where)
            ->where($where1)
            ->order($sort, $order)
            ->count();
        $list = $this->model
            ->with('seconditem')
            ->where($where)
            ->where($where1)
            ->order($sort, $order)
            ->limit($offset, $limit)
            ->select()
            ;

        $list = collection($list)->toArray();
        $result = array("total" => $total, "rows" => $list);

        $this->success('查询跟进房源操作成功',$result);
    }
}