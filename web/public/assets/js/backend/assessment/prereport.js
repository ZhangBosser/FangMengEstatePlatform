define(['jquery', 'bootstrap', 'backend', 'table', 'form','umeditor','moment','backend/stringutils'], 
function ($, undefined, Backend, Table, Form,UME,Moment,stringutils) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'assessment/prereport/index' + location.search,
                    table: 'assessment_prereport',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'item_id',
                sortName: 'id',
                columns: [
                    [
                        {field: 'prereport_no', title: '预评报告编号'},
                        {field: 'assessmentitems.obligee', title: '权利人'},
                        {field: 'assessmentitems.address', title: '地址'},
                        {field: 'version', title: '版本号'},
                        {field: 'admin.nickname',title:'预评员'},
                        {field: 'create_time', title: '提交时间', operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,formatter: Table.api.formatter.operate, buttons: [
                            {
                                name: 'detail',
                                text: '预览',
                                title: '预览',
                                icon: 'fa fa-list',
                                classname: 'btn btn-xs btn-success btn-dialog',
                                url:'/admin2020.php/assessment/prereport/previewpdf'
                            },
                            {
                                name: 'detail',
                                text: '下载',
                                title: '下载',
                                icon: 'fa fa-download',
                                classname: 'btn btn-xs btn-success',
                                url:'/admin2020.php/assessment/prereport/downloadpdf'
                            }
                        ]}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add1: function () {
            var editor;
            var source;
            // 初始化UME编辑器
            var load = function(){
                console.log('run load 1');
                if(!editor){
                    console.log('run load 2 init UME');
                    editor=UME.getEditor('c-prereport_contents');
                    source = editor.getContent();
                }
                replace();
            };

            // 自定义标签替换
            var replace=function(){
                var contents = source.replace(/\$\{PREREPORT_NO\}/g,$('#c-prereport_no').val());

                var _unit = $('#c-prereport_unit').val();
                $('#c-prereport_unit').val(parseFloat($('#c-prereport_unit').val()));
                var _total = parseFloat(_unit) * area/10000;
                var _round = Math.round(_total*100)/100;
                $("#c-prereport_total").val(_round);

                contents = contents.replace(/\$\{PRICE_TOTLE_CHINESE\}/g,stringutils.number_chinese(_round*10000)); //总价中文
                contents = contents.replace(/\$\{PREREPORT_UNIT\}/g,$('#c-prereport_unit').val());  // 单价
                contents = contents.replace(/\$\{PREREPORT_TOTAL\}/g,$('#c-prereport_total').val());    // 预评总价
                if($('#c-prereport_time').val()){
                    contents = contents.replace('${PREREPORT_TIME}',$('#c-prereport_time').val());
                    var d = Moment($('#c-prereport_time').val());
                    var timestamp=d.valueOf();
                    d=new Date(timestamp);
                    contents = contents.replace(/\$\{DATE_UPPER}/g,stringutils.CNDateString(d));
                }
                console.log('replace');
                editor.setContent(contents);
            };
            // 自动重新加载
            $(".change").change(function(){
                load();
            });
            $("#link-viewversion").click(function(){
                Fast.api.open("assessment/prereport/versions?item_id=" + item_id, '查看版本变更记录', {
                    callback: function (data) {
                        renderFollow();
                    },
                    area:['90%','90%']
                });
                return false;
            });
        
            Controller.api.bindevent();
        },
        versions:function(){
            var min=0;
            $(".trLine").each(function(key,item){
                var tmp = parseInt($(item).data('id'));
                console.log('id=',tmp);
                if(tmp<min)min=tmp;
            });
            $(".trLine").click(function(){
                $(".contents").hide();
                console.log("#divBody-" + $(this).data('id'));
                $("#divBody-" + $(this).data('id')).fadeIn(400);
                // stringutils.TextCompare( $("#divBody-" + min), $("#divBody-" + $(this).data('id')));
            });
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});