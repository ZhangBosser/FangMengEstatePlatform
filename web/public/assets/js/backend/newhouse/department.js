define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'newhouse/department/index' + location.search,
                    add_url: 'newhouse/department/add',
                    edit_url: 'newhouse/department/edit',
                    del_url: 'newhouse/department/del',
                    multi_url: 'newhouse/department/multi',
                    table: 'newhouse_department_items',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'name', title: __('Name')},
                        {field: 'cop_id', title: __('Cop_id')},
                        {field: 'address', title: __('Address')},
                        {field: 'state', title: __('State')},
                        {field: 'create_time', title: __('Create_time'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'create_ip', title: __('Create_ip')},
                        {field: 'create_userid', title: __('Create_userid')},
                        {field: 'update_time', title: __('Update_time'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'update_ip', title: __('Update_ip')},
                        {field: 'update_userid', title: __('Update_userid')},
                        {field: 'authcops.cop_name', title: __('Authcops.cop_name')},
                        {field: 'admin.nickname', title: __('Admin.nickname')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});