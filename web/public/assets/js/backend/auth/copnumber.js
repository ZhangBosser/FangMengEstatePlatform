define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'auth/copnumber/index' + location.search,
                    add_url: 'auth/copnumber/add',
                    edit_url: 'auth/copnumber/edit',
                    del_url: 'auth/copnumber/del',
                    multi_url: 'auth/copnumber/multi',
                    table: 'auth_copnumber',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'authcops.cop_name', title: __('Cop_id')},
                        {field: 'tablename', title: __('Tablename')},
                        {field: 'year', title: __('Year')},
                        {field: 'max_number', title: __('Max_number')},
                        {field: 'primary_id', title: __('Primary_id')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});