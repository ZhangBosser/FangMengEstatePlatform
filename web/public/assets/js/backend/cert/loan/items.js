define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {
    var arr=['','业务终止','待签约','待抵押','待放款','业务完成'];
    var labels=['default','success','primary','info','info','default'];
    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cert/loan/items/index' + location.search,
                    add_url: 'cert/loan/items/add',
                    edit_url: 'cert/loan/items/edit',
                    del_url: 'cert/loan/items/del',
                    multi_url: 'cert/loan/items/multi',
                    table: 'cert_loan_items',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'loan_id',
                sortName: 'create_time',
                sortOrder: 'desc',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'loan_id', title: __('Loan_id'), visible:false},
                        {field:' item_no', title:__('Item_no')},
                        {field: 'buyer_name', title: __('Buyer_name')},
                        {field: 'seller_name', title: __('Seller_name')},
                        {field: 'address', title: __('Address')},
                        {field: 'total_price', title: __('Total_price'), operate:'BETWEEN'},
                        {field: 'down_payment', title: __('Down_payment'), operate:'BETWEEN'},
                        {field: 'loan_fee', title: __('Loan_fee'), operate:'BETWEEN'},
                        {field: 'area', title: __('Area'), operate:'BETWEEN'},
                        {field: 'fee', title: __('Fee'), operate:'BETWEEN'},
                        {field: 'create_userid', title: __('Create_userid')},
                        {field: 'create_time', title: __('Create_time'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'last_time', title: __('Last_time'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'certloanstate.state_name', title: __('Certloanstate.state_name')},
                        {field: 'admin.nickname', title: __('Admin.nickname')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate,
                        buttons:[
                            {
                                name:'view',
                                text:'查看',
                                title:'查看',
                                icon:'fa fa-book',
                                classname:'btn btn-xs btn-success btn-view btn-dialog',
                                url:'cert/loan/items/view'
                            }
                        ]}
                    ]
                ]
            });

               // 为表格绑定事件
               Table.api.bindevent(table);
               table.on('post-body.bs.table',function(){
                   $("a.btn-view").data('area',["80%","85%"]);
                //    $("a.btn-editone").data('area',["85%","85%"]);
               });
               table.on('dbl-click-row.bs.table', function (e, row, element, field) {
                   $(".btn-view", element).trigger("click");
               });
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        view: function(){
            var html="<span class='label label-" + labels[state] + "'>" + arr[state] + "</span>";
            $("#tdState").html(html);
            renderFollow();
         
            function renderFollow(){
                console.info('加载跟进');
                // 加载跟进
                $.post('cert/loan/follow',{item_id:item_id}, function(data, ret){
                    //成功的回调
                    var value=data.rows;
                    $("#ulFollow").empty();
                    var state;
                    for(var i=0;i<value.length;i++){
                        var tmp=value[i];
                        if(i==0)state=tmp.state;
                        var str='<li>[' + tmp.create_time + ']';
                        str +='[ '+arr[ tmp.state] + ' ]';
                        str += tmp.comment;
                        str += ' [ ' + tmp.admin.nickname + ' ] ';
                        str+='</li>';
                        $("#ulFollow").append(str);
                    }
                    if(state){
                        var html="<span class='label label-" + labels[state] + "'>" + arr[state] + "</span>";
                        $("#tdState").html(html);
                    }
                    return false;
                 });
            }

            $("#btnNewFollow").click(function(){
                Fast.api.open("cert/loan/follow/add?item_id=" + item_id + '&state=' + state, '新增跟进', {
                    callback: function (data) {
                        console.log('addfollow callback');
                        renderFollow();
                    }
                });
                return false;
            });
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});