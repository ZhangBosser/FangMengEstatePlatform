define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cert/loan/bank/index' + location.search,
                    add_url: 'cert/loan/bank/add',
                    edit_url: 'cert/loan/bank/edit',
                    del_url: 'cert/loan/bank/del',
                    multi_url: 'cert/loan/bank/multi',
                    table: 'cert_loan_bank',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'bank_id',
                sortName: 'bank_id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'bank_id', title: __('Bank_id')},
                        {field: 'bank_name', title: __('Bank_name')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});