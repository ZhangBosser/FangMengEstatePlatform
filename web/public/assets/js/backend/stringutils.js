define(function(){
     // 金额数字转中文
     function number_chinese(str) {
        var num = parseFloat(str);
        var strOutput = "",
            strUnit = '仟佰拾亿仟佰拾万仟佰拾元角分';
        num += "00";
        var intPos = num.indexOf('.');  
        if (intPos >= 0){
            num = num.substring(0, intPos) + num.substr(intPos + 1, 2);
        }
        strUnit = strUnit.substr(strUnit.length - num.length);
        for (var i=0; i < num.length; i++){
            strOutput += '零壹贰叁肆伍陆柒捌玖'.substr(num.substr(i,1),1) + strUnit.substr(i,1);
        }
        return strOutput.replace(/零角零分$/, '整').replace(/零[仟佰拾]/g, '零').replace(/零{2,}/g, '零').replace(/零([亿|万])/g, '$1').replace(/零+元/, '元').replace(/亿零{0,3}万/, '亿').replace(/^元/, "零元")
    }
    // 日期转中文
    function CNDateString(date) {
        var today = date;
        var chinese = [ '〇', '一', '二', '三', '四', '五', '六', '七', '八', '九' ];
        var y = today.getFullYear().toString();
        var m = (today.getMonth() + 1).toString();
        var d = today.getDate().toString();
        var result = "";
        for (var i = 0; i < y.length; i++) {
            result += chinese[y.charAt(i)];
        }
        result += "年";
        if (m.length == 2) {
            if (m.charAt(0) == "1") {
                result += "十";
                if (m.charAt(1) != "0") {
                    result += chinese[m.charAt(1)];
                }
                result += "月";
            }
        } else {
            result += (chinese[m.charAt(0)] + "月");
        }
        if (d.length == 2) {
            result += chinese[d.charAt(0)] + "十";
            if (d.charAt(1) != '0') {
                result += chinese[d.charAt(1)];
            }
            result += "日";
        } else {
            result += (chinese[d.charAt(0)] + "日");
        }
        return result.replace("月一十", "月十");
    }
    function TextCompare(dom1,dom2){
        if(!dom1&&!dom2){
            console.log('参数错误：dom1、dom2不能为空。');
            return;
        }
        else if(!dom1){
            //dom1为空：新增
            dom2.style.color = '#90EE90';
        }else if(!dom2){
            //dom2为空：删除
            dom1.style.color = '#FF6347';
            dom1.style.textDecoration = 'line-through';
        }else{
            //进行差异比较
            var result = _eq({value1:dom1.innerText||dom1.innerHTML,value2:dom2.innerText||dom2.innerHTML});
            dom1.innerHTML = result.value1;
            dom2.innerHTML = result.value2;
        }
    }
    function _eq(op) {
        if(!op){
            return op;
        }
        if(!op.value1_style){
            op.value1_style="background-color:#FEC8C8;";
        }
        if(!op.value2_style){
            op.value2_style="background-color:#FEC8C8;";
        }
        if(!op.eq_min){
            op.eq_min=3;
        }
        if(!op.eq_index){
            op.eq_index=5;
        }
        if (!op.value1 || !op.value2) {
            return op;
        }
        var ps = {
            v1_i: 0,
            v1_new_value: "",
            v2_i: 0,
            v2_new_value: ""
        };
        while (ps.v1_i < op.value1.length && ps.v2_i < op.value2.length) {
            if (op.value1[ps.v1_i] == op.value2[ps.v2_i]) {
                ps.v1_new_value += op.value1[ps.v1_i].replace(/</g,"<").replace(">",">");
                ps.v2_new_value += op.value2[ps.v2_i].replace(/</g,"<").replace(">",">");
                ps.v1_i += 1;
                ps.v2_i += 1;
                if (ps.v1_i >= op.value1.length) {
                    ps.v2_new_value += "<span style='" + op.value2_style + "'>" + op.value2.substr(ps.v2_i).replace(/</g,"<").replace(">",">") + "</span>";
                    break;
                }
                if (ps.v2_i >= op.value2.length) {
                    ps.v1_new_value += "<span style='" + op.value1_style + "'>" + op.value1.substr(ps.v1_i).replace(/</g,"<").replace(">",">") + "</span>";
                    break;
                }
            } else {
                ps.v1_index = ps.v1_i + 1;
                ps.v1_eq_length = 0;
                ps.v1_eq_max = 0;
                ps.v1_start = ps.v1_i + 1;
                while (ps.v1_index < op.value1.length) {
                    if (op.value1[ps.v1_index] == op.value2[ps.v2_i + ps.v1_eq_length]) {
                        ps.v1_eq_length += 1;
                    } else if (ps.v1_eq_length > 0) {
                        if (ps.v1_eq_max < ps.v1_eq_length) {
                            ps.v1_eq_max = ps.v1_eq_length;
                            ps.v1_start = ps.v1_index - ps.v1_eq_length;
                        }
                        ps.v1_eq_length = 0;
                        break;//只寻找最近的
                    }
                    ps.v1_index += 1;
                }
                if (ps.v1_eq_max < ps.v1_eq_length) {
                    ps.v1_eq_max = ps.v1_eq_length;
                    ps.v1_start = ps.v1_index - ps.v1_eq_length;
                }

                ps.v2_index = ps.v2_i + 1;
                ps.v2_eq_length = 0;
                ps.v2_eq_max = 0;
                ps.v2_start = ps.v2_i + 1;
                while (ps.v2_index < op.value2.length) {
                    if (op.value2[ps.v2_index] == op.value1[ps.v1_i + ps.v2_eq_length]) {
                        ps.v2_eq_length += 1;
                    } else if (ps.v2_eq_length > 0) {
                        if (ps.v2_eq_max < ps.v2_eq_length) {
                            ps.v2_eq_max = ps.v2_eq_length;
                            ps.v2_start = ps.v2_index - ps.v2_eq_length;
                        }
                        ps.v1_eq_length = 0;
                        break;//只寻找最近的
                    }
                    ps.v2_index += 1;
                }
                if (ps.v2_eq_max < ps.v2_eq_length) {
                    ps.v2_eq_max = ps.v2_eq_length;
                    ps.v2_start = ps.v2_index - ps.v2_eq_length;
                }
                if (ps.v1_eq_max < op.eq_min && ps.v1_start - ps.v1_i > op.eq_index) {
                    ps.v1_eq_max = 0;
                }
                if (ps.v2_eq_max < op.eq_min && ps.v2_start - ps.v2_i > op.eq_index) {
                    ps.v2_eq_max = 0;
                }
                if ((ps.v1_eq_max == 0 && ps.v2_eq_max == 0)) {
                    ps.v1_new_value += "<span style='" + op.value1_style + "'>" + op.value1[ps.v1_i].replace(/</g,"<").replace(">",">") + "</span>";
                    ps.v2_new_value += "<span style='" + op.value2_style + "'>" + op.value2[ps.v2_i].replace(/</g,"<").replace(">",">") + "</span>";
                    ps.v1_i += 1;
                    ps.v2_i += 1;

                    if (ps.v1_i >= op.value1.length) {
                        ps.v2_new_value += "<span style='" + op.value2_style + "'>" + op.value2.substr(ps.v2_i).replace(/</g,"<").replace(">",">") + "</span>";
                        break;
                    }
                    if (ps.v2_i >= op.value2.length) {
                        ps.v1_new_value += "<span style='" + op.value1_style + "'>" + op.value1.substr(ps.v1_i).replace(/</g,"<").replace(">",">") + "</span>";
                        break;
                    }
                } else if (ps.v1_eq_max > ps.v2_eq_max) {
                    ps.v1_new_value += "<span style='" + op.value1_style + "'>" + op.value1.substr(ps.v1_i, ps.v1_start - ps.v1_i).replace(/</g,"<").replace(">",">") + "</span>";
                    ps.v1_i = ps.v1_start;
                } else {
                    ps.v2_new_value += "<span style='" + op.value2_style + "'>" + op.value2.substr(ps.v2_i, ps.v2_start - ps.v2_i).replace(/</g,"<").replace(">",">") + "</span>";
                    ps.v2_i = ps.v2_start;
                }
            }
        }
        op.value1 = ps.v1_new_value;
        op.value2 = ps.v2_new_value;
        return op;
    }
    Date.prototype.format = function (fmt) {
        var o = {
            "M+": this.getMonth() + 1, //月份
            "d+": this.getDate(), //日
            "h+": this.getHours(), //小时
            "m+": this.getMinutes(), //分
            "s+": this.getSeconds(), //秒
            "q+": Math.floor((this.getMonth() + 3) / 3), //季度
            "S": this.getMilliseconds() //毫秒
        };
        if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var k in o)
            if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        return fmt;
    }
    return {number_chinese:number_chinese,CNDateString:CNDateString,TextCompare:TextCompare};
});