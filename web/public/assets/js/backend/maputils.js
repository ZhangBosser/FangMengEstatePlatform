/**
 * require路径 ,'backend/maputils'
 */
define(function(){
    var map;
    var marker;
    var point;
    var myGeo;
    var init=function(cop_map_container,lng,lat,callback){
        map = new BMap.Map(cop_map_container);
        map.addControl(new BMap.NavigationControl());    
        map.addControl(new BMap.ScaleControl());    
        map.addControl(new BMap.OverviewMapControl());    
        map.addControl(new BMap.MapTypeControl());    
        map.setCurrentCity("合肥"); // 仅当设置城市信息时，MapTypeControl的切换功能才能可用  

        point = new BMap.Point(lng,lat);
        // 创建点坐标  
        map.centerAndZoom(point, 16);
        // 初始化地图，设置中心点坐标和地图级别  
        marker = new BMap.Marker(point);        // 创建标注    
        map.addOverlay(marker);                     // 将标注添加到地图中 
        marker.enableDragging();
        myGeo = new BMap.Geocoder();
        marker.addEventListener("dragend", function(e){
            console.log('dragend to',e.point);
            myGeo.getLocation(e.point, function(result){      
                console.log('geo getlocation result',result);
                callback(e,result);
            });
        });
        callback({point:{lat:lat,lng:lng}});
    };

    var getLocation=function(point,callback){
        myGeo.getLocation(point,callback);
    }
    var position=function(lng,lat){
        map.centerAndZoom(new BMap.Point(lng,lat))
    };
    /**
     * 根据地址定位
     * @param {地址} address 
     * @param {回调函数} callback 
     */
    var geo=function(address,callback){
        myGeo = new BMap.Geocoder();
        myGeo.getPoint(address, function(point){      
            if (point) {      
                map.centerAndZoom(point, 16);      
                marker.setPosition(point);
                myGeo.getLocation(point, function(result){      
                    console.log('geo getlocation result',result);
                    callback(point,result);
                });
            }      
        }, 
        "合肥市");
    }
    
    /**
     * 示例 map.position(31.866842,117.282699,"cop-map-container","c-longitude","c-latitude","c-address","c-building_name");
     * @param {纬度}} lat 
     * @param {经度} lng 
     * @param {地图容器的id} cop_map_container 
     * @param {经度控件的id} longitude_id 
     * @param {纬度控件的id} latitude_id 
     * @param {返回地址的控件id} address_id 
     * @param {初始定位控件的id} brief_address_id 
     */
    // var bind= function(longitude_id,latitude_id,address_id,brief_address_id){
       
      
    //     $("a.link-map").click(function(){
    //         if(brief_address_id && !$("input#" + brief_address_id).val())return;
    //         var myGeo = new BMap.Geocoder();
    //         // 将地址解析结果显示在地图上，并调整地图视野    
    //         myGeo.getPoint($("input#" + brief_address_id).val(), function(point){      
    //             if (point) {      
    //                 map.centerAndZoom(point, 16);      
    //                 marker.setPosition(point);
    //             //	map.addOverlay(new BMap.Marker(point));
    //                 $("input#" + longitude_id).val(point.lng);
    //                 $("input#" + latitude_id).val(point.lat);
    //                 myGeo.getLocation(point, function(result){      
    //                     if (result && address_id){      
    //                         $("input#" + address_id).val(result.address);
    //                     }      
    //                 });
    //             }      
    //         }, 
    //         "合肥市");

    //     });
    // }
    return {
        position:position,
        init:init,
        marker:marker,
        getLocation:getLocation,
        geo:geo
    };
});