/**
 * 连接websocket扫描网址变化
 */
define(['jquery', 'bootstrap', 'backend','/assets/js/backend/socketio/socket.io.js'],
    function($, undefined, Backend,io){
    var run=function(){
        start();
    };
    var start=function(){
        var socket = io.connect('http://localhost:5000/app');
        //连接服务端，因为本机使用localhost 所以connect(url)中url可以不填或写 http://localhost

        // 监听 receiveMsg 事件，用来接收其他客户端推送的消息
        socket.on("receiveMsg",function (data) {
            console.log(data.client , data.msg);
        });
        socket.on("connect",function (data) {
            console.log('connect', data);
          
        });
        socket.on("response",function(data){
            console.log('response', data);
            if(data.ret=='alert'){
                Toastr.success('网上有新房源');
            }
            // var data={client:"client-1",msg:'shake hand'};
            //给服务端发送 sendMsg事件名的消息
            // console.log('send to server',data);
            // socket.emit("sendMsg",data);
        });

    };
    return {run:run};
});