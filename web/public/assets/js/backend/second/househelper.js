define(function(){
    /**
     * 取电话
     * @param {输入文本} input 
     */
    function getMobile(txt){
        var patrn = /[0-9]{7,20}/gi;
        var arr;
        var ret={txt:txt};

        if (patrn.test(txt)) {
            arr = txt.match(patrn);
            if (arr.length > 0) {
                ret.mobile=arr[0];
            }
            ret.txt = txt.replace(patrn,'');
        }
        console.debug('mobile result',ret);
        return ret;
    }
    /**
     * 取总价
     * @param {输入文本}} txt 
     */
    function getPriceTotal(txt){
        ///取总价
        var ret={txt:txt};
        var patrn =[
            /([0-9]*(.[0-9])?)万/,
            / +[\d|\.]+ +/
        ]
        for(var j in patrn){
            var item = patrn[j];
            if (item.test(txt)) {
                arr = txt.match(item);
                var temp = parseFloat( arr[0].replace("万", ""));
                if(temp>10)
                ret.priceTotal = temp;
                ret.txt = txt.replace(item,'');
                break;
            }
        }
        if(!ret.priceTotal){
            console.error('无法取得总价');
        }
        console.debug('priceTotal result',ret);
        return ret;
    }
    /**
     * 取小区名称
     * @param {输入文本} txt 
     */
    function getBuildingName(txt){
        //取小区
        var patrn = /[\u4e00-\u9fa5·]{3,20}/g;
        var ret={txt:txt};
        if (patrn.test(txt)) {
            var arr = txt.match(patrn);
            var j=0;

            if(arr.length>1){
                //赵先生 省委大院 2室1厅 65.0平米 210  6/6 简单装修83#607 刚下证  13905601 名称在前面的情况
                for(var i in arr){
                    var item=arr[i];
                    if(item.indexOf('先生')>-1 || item.indexOf('女士')>-1 || item.indexOf('姐')>-1 || item.indexOf('哥')>-1 || item.indexOf('经理')>-1 || item.indexOf('老板')>-1){
                        
                    }else{
                        j=i;break;
                    }
                }
            }
            ret.buildingName = arr[j].replace('·', '');
            ret.txt          = txt.replace(ret.buildingName,'');
        }
        console.info('buildingName result',ret);
        return ret;
    }
    /**
     * 取人名
     * @param {输入文本} txt 
     */
    function getCustomerName(txt){
        console.log('customer input txt',txt);
        //联系人
        var patrn =[
                 /[\u4e00-\u9fa5·][女士|先生][\u4e00-\u9fa5·]/,
                 /[\u4e00-\u9fa5·][\u4e00-\u9fa5·][大姐]/,
                 /[\u4e00-\u9fa5·][\u4e00-\u9fa5·][小姐]/,
                 /[\u4e00-\u9fa5·][\u4e00-\u9fa5·][姐]/,
                 /[\u4e00-\u9fa5·][\u4e00-\u9fa5·][大哥]/,
                 /[\u4e00-\u9fa5·][\u4e00-\u9fa5·][哥]/,
                 /xs/i,
                 /ns/i,
                 /先生/,
                 /女士/,
                 /经理/,
                 /[\u4e00-\u9fa5]{1,3}/
                ];
        var ret={txt:txt};

        for(var p in patrn){
            var item = patrn[p];
            if(item.test(txt)){
                arr = txt.match(item);
                ret.customerName = arr[0];
                ret.txt = txt.replace(item,'');
                break;
            }
        }
        if(!ret.customerName){
            console.error('无法取得姓名,赋值默认先生');
            ret.customerName='先生';
        }
        if(ret.customerName){
            ret.customerName=ret.customerName.replace(/ /g,'');
            var ignore=['尾款','无尾款','欠款','产证面','产证','在卖'];
            for(var i in ignore){
                if(ret.customerName==ignore[i]){
                    ret.customerName='先生';
                }
            }
        }

        console.debug('customerName result',ret);
        return ret;
    }
    /**
     * 取面积，并根据面积计算厅室数量
     * @param {输入文本} txt 
     */
    function getArea(txt){
        ///取面积
        txt=txt.replace('平米','平');
        txt=txt.replace('平方','平');
        txt=txt.replace('平方米','平');
        var patrn = /([\d|\.]{2,6})[m|平]/;
        var ret={txt:txt};
        if (patrn.test(txt)) {
            arr = txt.match(patrn);
            var acre = arr[0].replace("m", "").replace("平", "").replace(" ", "");
            acre = parseFloat(acre);
            if (acre > 20 && acre < 200) {
                if (acre <= 90) {
                    ret.shiCount = 2;
                    ret.tingCount = 1;
                }
                else if (acre > 90 && acre < 130) {
                    ret.shiCount = 3;
                    ret.tingCount = 2;
                }
                else {
                    ret.shiCount = 4;
                    ret.tingCount = 2;
                }
                ret.area = acre;
            }
            ret.txt = txt.replace(patrn,'');
            console.info('getArea result:',ret);
            return ret;
        }else{
            console.error('无法取得面积');
            return null;
        }
    }
    /**
     * 几室几厅
     * @param {输入文本} txt 
     */
    function getShiTing(txt){
        var ret={txt:txt};

        //取几室几厅,中文
        var patrn = /([\u4e00-\u9fa5])室([\u4e00-\u9fa5])厅/g;
        if(patrn.test(txt)){
            var arr = txt.match(patrn);
            var _shi = RegExp.$1;
            var _ting = RegExp.$2;
            var cn='零一二三四五六七八九十';
            ret.shiCount = cn.indexOf(_shi);
            ret.tingCount= cn.indexOf(_ting);
            ret.txt = txt.replace(patrn,"");

            // 取楼层
            patrn = /[\d]+([\/-]\d+)+/gi;
            if (patrn.test(txt)) {
                arr = txt.match(patrn);
                console.log('shiting',arr);
                for (var i = 0; i < arr.length; i++) {
                    var arr1 = arr[i].match(/\d+/gi);
                    ret.floorTotal  =  parseInt(arr1[1]);
                    if(!ret.floor) ret.floor =  parseInt(arr1[0]);
                }
            }
        }else{
            // 阿拉伯数字
            patrn = /\d室\d厅/gi;
            if(patrn.test(txt)){
                var arr=txt.match(patrn);
                for (var i = 0; i < arr.length; i++) {
                    var arr1 = arr[i].match(/\d/gi);
                    ret.shiCount    = parseInt( arr1[0]);
                    ret.tingCount   = parseInt( arr1[1]);
                }
                ret.txt = txt.replace(patrn,'');
                // 取楼层
                patrn = /[\d]+([\/-]\d+)+/gi;
                if (patrn.test(txt)) {
                    arr = txt.match(patrn);
                    console.log('shiting',arr);
                    for (var i = 0; i < arr.length; i++) {
                        var arr1 = arr[i].match(/\d+/gi);
                        ret.floorTotal  =  parseInt(arr1[1]);
                        if(!ret.floor) ret.floor =  parseInt(arr1[0]);
                    }
                }
            }else{
                patrn = /[\d]([\/-]\d+)+/gi;

                if (patrn.test(txt)) {
                    arr = txt.match(patrn);
                    console.log('shiting',arr);
                    for (var i = 0; i < arr.length; i++) {
                        var arr1 = arr[i].match(/\d+/gi);
                    
                        if ((arr1[0] == "2" && arr1[1] == "2") || parseInt(arr1[0]) <= parseInt(arr1[1])) {
                            //区分层
                            ret.floorTotal  =  parseInt(arr1[1]);
                            ret.floor       =  parseInt(arr1[0]);
                        }
                        else {
                            ret.shiCount  =  parseInt(arr1[0]);
                            ret.TingCount =  parseInt(arr1[1]);
                        }
                    }
                    ret.txt = txt.replace(patrn,'');
                }
            }

            console.info('getShiTing result:',ret);
        }
        
        return ret;
    }
    /**
     * 几栋几零几
     * @param {输入文本} txt 
     */
    function getDongShi(txt){
        var ret={txt:txt};
        //栋号房号
        var patrn = /[\d\w]+ ?[#|栋| ]?[\d]{2,5} /ig;
        if (patrn.test(txt)) {
            arr = txt.match(patrn);
            var s=0;
            if(arr.length>1){
                // 赵先生 省委大院 2室1厅 65.0平米 210  6/6 简单装修83#607 刚下证  1390**** 这种格式，优先匹配含#的
                for(var i in arr){
                    if(arr[i].indexOf('#')>-1 || arr[i].indexOf('栋')>-1 || arr[i].indexOf('幢')>-1){
                        s=i;break;
                    }
                }
            }
            var arr1 = arr[s].match(/[\d\w]+/ig);

            ret.dong = arr1[0];
            ret.shi  = arr1[1];
            if(ret.shi){
                if(ret.shi.length==3)
                    ret.floor = ret.shi[0];
                else if(ret.shi.length>3)
                    ret.floor = ret.shi[0] + ret.shi[1];
            }
            ret.txt = ret.txt.replace( arr[s],"");
        }else{
            console.error('无法取得几栋几零几');
        }
        console.info('getDongShi result:',ret);
        return ret;
    }

    /**
     * 装修
     */    
    function getDecoration(txt){
        var ret={txt:txt};
        var decoration;
        if(txt.indexOf('简装')>-1)          decoration='简装';
        else if(txt.indexOf('精装')>-1)     decoration='精装';
        else if(txt.indexOf('豪装')>-1)     decoration='豪装';
        else if(txt.indexOf('豪华装修')>-1) decoration='豪装';
        else if(txt.indexOf('毛坯')>-1)     decoration='毛坯';
        else                                decoration='未知';

        ret.txt=txt.replace(decoration,'');

        var temparr=['未知','毛坯','简装','精装','豪装'];
        ret.decoration = temparr.indexOf(decoration);
        return ret;
    }
    /**
     * 文本解析
     * @param {输入文本} txt 
     */
    function analysisInfo(txt) {
        txt=txt.replace('先生','先生 ');
        txt=txt.replace('女士','女士 ');
        txt=txt.replace('大姐','大姐 ');
        txt=txt.replace('小姐','小姐 ');
        txt=txt.replace('经理','经理 ');
        txt=txt.replace('老板','老板 ');
        txt=txt.replace('大哥','大哥 ');
        txt=txt.replace('# ','');
        while(txt.indexOf('  ')>-1){
            txt=txt.replace('  ',' ');
        }
        
        var ret={};

        // 装修
        $.extend(ret,getDecoration(txt));
        txt = ret.txt;

        $.extend(ret, getMobile(txt));
        txt = ret.txt;
        
        $.extend(ret, getDongShi(txt));
        txt = ret.txt;

        $.extend(ret,getBuildingName(txt));
        txt = ret.txt;

        $.extend(ret, getArea(txt));
        txt = ret.txt;

        $.extend(ret,getShiTing(txt));
        txt=ret.txt;

        // 计算总价
        $.extend(ret, getPriceTotal(txt));
        console.log('pricetotal after',ret);
        txt = ret.txt;
        
        $.extend(ret,getCustomerName(txt));
        delete(ret.txt);
        console.log(ret);
        return ret;
        // queryBuildInfo($("input[name=HouseSeQv]").val());
        // querySimilar($("input[name=HouseSeQv]").val(), $("input[name=base_danyuan]").val(), $("input[name=base_shi]").val());
    }
    return {analysisInfo:analysisInfo};
});