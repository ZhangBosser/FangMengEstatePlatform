define(['jquery', 'bootstrap', 'backend', 'table', 'form','backend/maputils'], function ($, undefined, Backend, Table, Form,map) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'second/buildings/index' + location.search,
                    add_url: 'second/buildings/add',
                    edit_url: 'second/buildings/edit',
                    del_url: 'second/buildings/del',
                    multi_url: 'second/buildings/multi',
                    table: 'second_buildings',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                search:true,
                sortName: 'id',searchFormVisible: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'building_name', title: __('Building_name'),operate: 'LIKE'},
                        {field: 'latitude', title: __('Latitude'), operate:'BETWEEN'},
                        {field: 'longitude', title: __('Longitude'), operate:'BETWEEN'},
                        {field: 'areaName', title: __('areaName')},
                        {field: 'alias_name1', title: __('Alias_name1')},
                        {field: 'alias_name2', title: __('Alias_name2')},
                        {field: 'second_count', title: __('Second_count')},
                        {field: 'second_onsell', title: __('Second_onsell')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ],onPostBody:function(arg1){
                    console.log('onpostbody');
                    $(".btn-editone").data('area',['95%','85%']);
                }
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            map.init("cop-map-container",117.282699,31.866842,function(e){
                $("input#c-longitude").val(e.point.lng);
                $("input#c-latitude").val(e.point.lat);
                map.getLocation(e.point,function(result){
                    if(result){
                        $("#c-address").val(result.address); 
                        //TODO: areacode赋值 result.addressComponents.province city district streat streetNumber
                        $("#c-business").val(result.business);
                    }
                });
            });

            $("input#btn-map").click(function(){
                map.geo($("#c-building_name").val(),function(point,ret){
                    $("input#c-latitude").val(point.lat);
                    $("input#c-longitude").val(point.lng);
                    $("input#c-address").val(ret.address);
                    $("#c-business").val(ret.business);
                });
            });
            $("#c-areaName").on("cp:updated", function() {
                var citypicker = $(this).data("citypicker");
                var code = citypicker.getCode("district") || citypicker.getCode("city") || citypicker.getCode("province");
                $("#c-areaCode").val(code);
              });
            Controller.api.bindevent();
        },
        edit: function () {
            map.init("cop-map-container",longitude,latitude,function(e){
                $("input#c-longitude").val(e.point.lng);
                $("input#c-latitude").val(e.point.lat);
            });

            $("input#btn-map").click(function(){
                map.geo($("#c-building_name").val(),function(point,ret){
                    $("input#c-latitude").val(point.lat);
                    $("input#c-longitude").val(point.lng);
                    $("input#c-address").val(ret.address);
                    $("#c-business").val(ret.business);
                });
            });
            $("#c-areaName").on("cp:updated", function() {
                var citypicker = $(this).data("citypicker");
                var code = citypicker.getCode("district") || citypicker.getCode("city") || citypicker.getCode("province");
                $("#c-areaCode").val(code);
              });
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        },
    //     position: function(lat,lng){
    //         //地图
    //         console.log('position');
    //         var map = new BMap.Map("cop-map-container");
    //         //
    //         map.addControl(new BMap.NavigationControl());    
    //         map.addControl(new BMap.ScaleControl());    
    //         map.addControl(new BMap.OverviewMapControl());    
    //         map.addControl(new BMap.MapTypeControl());    
    //         map.setCurrentCity("合肥"); // 仅当设置城市信息时，MapTypeControl的切换功能才能可用  

    //         // 创建地图实例  
    //         var point = new BMap.Point(lng,lat);
    //         // 创建点坐标  
    //         map.centerAndZoom(point, 15);
    //         // 初始化地图，设置中心点坐标和地图级别  
    //         var marker = new BMap.Marker(point);        // 创建标注    
    //         map.addOverlay(marker);                     // 将标注添加到地图中 
    //         marker.enableDragging();    
    //         marker.addEventListener("dragend", function(e){    
    //             $("input#c-longitude").val(e.point.lng);
    //             $("input#c-latitude").val(e.point.lat);
    //         });
            
    //         $("a.link-map").click(function(){
    //             if(!$("input#c-building_name").val())return;
    //             var myGeo = new BMap.Geocoder();
    //             // 将地址解析结果显示在地图上，并调整地图视野    
    //             myGeo.getPoint($("input#c-building_name").val(), function(point){      
    //                 if (point) {      
    //                     map.centerAndZoom(point, 16);      
    //                     marker.setPosition(point);
    //                 //	map.addOverlay(new BMap.Marker(point));
    //                     $("input#c-longitude").val(point.lng);
    //                     $("input#c-latitude").val(point.lat);
    //                     myGeo.getLocation(point, function(result){      
    //                         if (result){      
    //                             $("input#c-address").val(result.address);
    //                         }      
    //                     });
    //                 }      
    //             }, 
    //             "合肥市");

    //         });
    //     }
     };
    return Controller;
});