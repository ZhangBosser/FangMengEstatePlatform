/**
 * textarea处理类
 */
define(['jquery', 'bootstrap', 'backend','/assets/js/backend/socketio/socket.io.js'],
    function($, undefined, Backend,io){
        var Controller={
            // 文本内容转数组
            getArray:function($textBox){
                var val = $textBox.val();
                var array = val.split('\n');
                return array;
            },
            // 移除空行
            removeBlank:function($textBox){
                var array = this.getArray($textBox);
                // 移除空行
                var removeblank =[];
                for(var s in array){
                    if(array[s]){
                        if(/\d{1,2}:\d{1,2}:\d{1,2}/.test(array[s])){ //qq消息的时间格式

                        }else if(array[s]=='.' || array[s]==' '){

                        }
                        else{
                            removeblank.push(array[s]);
                        }
                    }else{
                        //removeblank.push(array[s]);
                    }
                }
                $textBox.val(removeblank.join('\n'));
                return removeblank;
            },
            // 获取当前光标位置
            getLineNum:function($textBox){
                var start = $textBox[0].selectionStart;
                var array = this.getArray($textBox);
                var i=0;
                var count = 0;
                while(i<array.length){
                    count += array[i].length;
                    i++;
                    if(start<count) break;
                }
                return i;
            },
            // 选择下一行，如果已经最后一行了就返回false
            moveNextRow:function($textBox){
                var array = this.getArray($textBox);
                var line = this.getLineNum($textBox);
                if(line<array.length){
                    line++;
                    this.selectRow($textBox,line);
                    console.log('line num=',line,array[line-1]);
                    return array[line-1];
                }else{
                    return false;
                }
            },
            /**选择第几行 */
            selectRow($textBox,row_num){
                console.log('select row_num',row_num);
                var array = this.getArray($textBox);

                var count=[];
                for(var row in array){
                    var line=array[row];
                    count[row] = line.length;
                }
                var start=0,end=0;
                for(var i=0;i<row_num;i++){
                    if(i>0){
                        start += count[i-1] +1 ;
                        end += 1; // 换行
                    }
                    end   += count[i];
                }
                if(end==0){
                    console.error('没有计算出文本位置');
                    return;
                }
                $textBox.scrollTop(parseInt($textBox.css('line-height'))*(row_num-1));
                this.textSelect($textBox,start,end);
                return array[row_num-1];
            },
            /**选择一段文本 */
            textSelect($textBox,start,end){
                if(!$textBox.val()){
                    console.info('文本内容为空');
                    return;
                }
                var textBox = $textBox[0];
                if(textBox.setSelectionRange){
                    textBox.setSelectionRange(start,end);	
                }else if(textBox.createTextRange){
                    var rang = textBox.createTextRange();
                    rang.collapse(true);
                    rang.moveStart('character',start);
                    rang.moveEnd('character',end-start);
                    rang.select();	
                }
                textBox.focus();
            }
        };
        return Controller;
    });