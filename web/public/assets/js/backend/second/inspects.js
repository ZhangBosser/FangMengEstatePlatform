/**
 * 获取网页变化
 */
define(['jquery', 'bootstrap', 'backend', 'table', 'form','backend/maputils','backend/second/househelper','backend/second/webinspect'],
 function ($, undefined, Backend, Table, Form,map,helper,inspect) {
    var list= function () {
        // 初始化表格参数配置
        Table.api.init({
            extend: {
                index_url: 'http://localhost:5000/inspects/list' + location.search,
                del_url: 'second/inspects/del',
                table: 'second_inspects',
            }
        });

        var table = $("#table");

        // 初始化表格
        table.bootstrapTable({
            url: $.fn.bootstrapTable.defaults.extend.index_url,
            pk: 'id', pageSize: 20,
            sortName: 'update_time',
            sortOrder: "desc",  
            dblClickToEdit:false,
            columns: [
                [
                    {checkbox: true},
                    {field: 'id', title: __('Id')},
                    {field: 'url', title:'链接',operate: 'LIKE'},
                    {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,
                    buttons:[
                        {
                            name:'view',
                            text:'查看',
                            title:'查看',
                            icon:'fa fa-building',
                            classname:'btn btn-xs btn-success btn-view btn-dialog',
                            url:'second/items/view'
                        }
                    ],formatter: Table.api.formatter.operate
                }
                ]
            ]
        });

        // 为表格绑定事件
        Table.api.bindevent(table);
        table.on('post-body.bs.table',function(){
            $("a.btn-view").data('area',["90%","95%"]);
            $("a.btn-editone").data('area',["85%","85%"]);
        });
        table.on('dbl-click-row.bs.table', function (e, row, element, field) {
            $(".btn-view", element).trigger("click");
        });
        /**
         * 监控网页内容变化
         */
        inspect.run();
    }
    return {list:list};
});