define(['jquery', 'bootstrap', 'frontend', 'form', 'template'], function ($, undefined, Frontend, Form, Template) {
    var validatoroptions = {
        invalid: function (form, errors) {
            $.each(errors, function (i, j) {
                Layer.msg(j);
            });
        }
    };
    var Controller = {
        book2:function(){

            var characters=
                '一,二,三,上,口,目,耳,手,日,田,禾,火,'+ 
                // 一年级上学期
                '虫,云,山,八,十,'+
                '了,子,人,大,月,儿,头,里,可,东,西,天,四,' +
                '是,女,开,水,去,来,不,小,少,牛,果,鸟,' +
                '早,书,刀,尺,本,木,林,土,力,心,中,五,立,正,'+
                '在,后,我,好,长,比,巴,把,下,个,雨,们,'+
                '问,有,半,从,你,才,明,同,学,自,己,衣,' +
                '白,的,又,和,竹,牙,马,用,几,只,石,多,出,见,'+
                '对,妈,全,回,工,厂,' +
            // 一年级上学期识字表
                '天,地,人,你,我,他,四,五,上,下,风,花,' +
                '六,七,九,爸,画,棋,鸡,字,词,语,' +
                '句,桌,纸,文,数,学,音,乐,' +
                '妹,奶,白,皮,小,桥,台,雪,草,家,是,车,路,灯,走,'+
                '秋,气,了,树,叶,片,大,飞,会,个,'+
                '的,船,两,头,在,里,看,见,闪,星,'+
                '江,南,可,采,莲,鱼,东,西,北,'+
                '尖,说,春,青,蛙,夏,弯,地,就,冬,' +
                '男,女,开,头,正,反,远,有,色,近,听,无,声,去,还,来,' +
                '多,少,黄,牛,只,猫,边,鸭,苹,果,杏,桃,'+
                '书,包,尺,作,业,本,笔,刀,课,早,校,'+
                '明,力,尘,从,众,双,木,林,森,条,心,'+
                '升,国,旗,中,红,歌,起,么,美,丽,立,'+
                '午,晚,昨,今,年,' +
                '影,前,后,黑,狗,左,右,它,好,朋,友,'+
                '比,尾,巴,谁,长,短,把,伞,兔,最,公,'+
                '写,诗,点,要,过,给,当,串,们,以,成,'+
                '数,彩,半,空,问,到,方,没,更,绿,出,长,'+
                '睡,那,海,真,老,师,吗,同,什,才,亮,'+
                '时,候,觉,得,自,己,很,穿,衣,服,快,'+
                '蓝,又,笑,着,向,和,贝,娃,挂,活,金,'+
                '哥,姐,弟,叔,爷,'+
                '群,竹,牙,用,几,步,为,参,加,洞,着,'+
                '乌,鸦,处,找,办,旁,许,法,放,进,高,'+
                '住,孩,玩,吧,发,芽,爬,呀,久,回,全,变,'+
                '工,厂,医,院,生,'+
            // 一年级下学期
                '霜,吹,落,降,飘,游,池,入,'+
                '姓,氏,李,张,古,吴,赵,钱,孙,周,王,官,'+
                '清,晴,眼,睛,保,护,害,事,情,请,让,病,'+
                '相,遇,喜,欢,怕,言,互,令,动,万,纯,净,'+
                '阴,雷,电,阵,冰,冻,夹,'+
                '吃,忘,井,村,叫,毛,主,席,乡,亲,战,士,面,'+
                '想,告,诉,京,安,门,非,常,壮,观,'+
                '接,觉,再,做,各,种,样,伙,伴,却,也,趣,这,'+
                '太,阳,道,送,忙,尝,香,甜,温,暖,该,颜,因,'+
                '辆,匹,册,支,铅,棵,架,'+
                '块,捉,急,直,河,行,死,信,跟,忽,喊,身,'+
                '只,窝,孤,单,种,都,邻,居,招,呼,静,乐,'+
                '怎,独,跳,绳,讲,得,羽,球,戏,排,篮,连,运,'+
                '夜,思,床,光,疑,举,望,低,故,'+
                '胆,敢,往,外,勇,窗,乱,偏,散,原,像,微,'+
                '端,粽,节,总,米,间,分,豆,肉,带,知,据,念,'+
                '虹,座,浇,提,洒,挑,兴,镜,拿,照,千,裙,'+
                '眉,鼻,嘴,脖,臂,肚,腿,脚,'+
                '蜻,蜓,迷,藏,造,蚂,蚁,食,粮,蜘,蛛,网,'+
                '圆,严,寒,酷,暑,凉,晨,细,朝,霞,夕,杨,'+
                '操,场,拔,拍,跑,踢,铃,热,闹,锻,炼,体,'+
                '之,初,性,善,习,教,迁,贵,专,幼,玉,器,义,' +
                '首,采,无,树,爱,尖,角,亮,机,台,放,鱼,朵,美,' +
                '直,呀,边,呢,吗,吧,加,文,次,找,平,办,让,包';

                /**
                 * 去除相同元素
                 */
                Array.prototype.unique=function(){
                    var arr=this;
                    var hash=[];
                    for (var i = 0; i < arr.length; i++) {
                        if(hash.indexOf(arr[i])==-1){
                        hash.push(arr[i]);
                        }
                    }
                    return hash;
                };

                /**
                 * 数组移除方法
                 */
                Array.prototype.remove = function(val) { 
                    var index = this.indexOf(val); 
                    if (index > -1) { 
                        this.splice(index, 1); 
                    } 
                };

                /**
                 * 所有生字
                 */
                var array_all;

                /**记录频率 */
                var rate = {};
              
                var array;
              
                /// 当前显示第几个
                var showIndex=0;

                /**
                 * 初始化
                 */
                var init =function(){
                    var tempArray = characters.split(',');
                    array_all=tempArray.unique();
                    console.log('共有:' + tempArray.length + '个，去重后有:' + array_all.length + '个');
                    for(var s in array_all){
                        rate[array_all[s]]={show:0,yes:0,no:0,letter:''};
                    }
                    if(localStorage.array){
                        array=localStorage.array.split(',');
                    }else{
                        array = array_all;
                    }
                    if(localStorage.rate){
                        rate=JSON.parse(localStorage.rate);
                    }
    
                };
                var show=function(){
                    $("#board").text(array[showIndex]);
                    $("#count").text(array.length);
                    /// 全部生字表
                    var temp=Template('tempAll',{array:array_all,rate:rate});
                    $("#all").html(temp);
                    console.log(rate);
                };

                /**
                 * 显示下一个
                 */
                var next=function(){
                    var from=0,to=array.length;
                    var num = Math.floor(Math.random()*(from - to) + to);
                    showIndex=num;
                    var letter = array[showIndex];
                   
                    show();
                };
                /**
                 * 会了的移除
                 */
                var removeOne=function(index){
                    array.remove(characters[index]);
                 
                };
                /**
                 * 保存到本地
                 */
                var save=function(){
                    localStorage.array=array.join(',');
                    if(rate){
                        localStorage.rate =JSON.stringify( rate);
                    }
                };
     
                var yes=0,no=0; // 会与不会的数量
                init();
                next();
                $(".btn-ret").click(function(){
                    
                    var ret=$(this).data('ret');
                    var letter = array[showIndex];
                    if(ret){
                        yes++;
                        rate[letter].yes++;
                        $("#yes").text(yes);
                        if(rate[letter].yes==3){
                            console.log('会了 index=',showIndex, array[showIndex]);
                            removeOne(showIndex);
                        }
                    }else{
                        no++;
                        rate[letter].no++;
                        $("#no").text(no);
                    }
                    rate[letter].letter=letter;
                    rate[letter].show++;
                    save();
                    next();
                });
                $("#btnReset").click(function(){
                    localStorage.clear();
                    init();
                    next();
                });
        }
    };
    return Controller;
});