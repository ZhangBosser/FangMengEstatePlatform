define(['jquery', 'bootstrap', 'frontend', 'form', 'template','table','backend','backend/maputils'], 
function ($, undefined, Frontend, Form, Template,Table,Backend,map) {
    var arr=['在售','停售','已售','无效'];
    var labels=['success','primary','info','default'];

    var Controller = {
        
        add:function(){
            Controller.api.bindevent();
        },
        
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"), function(data, ret){
                    //这里是表单提交处理成功后的回调函数，接收来自php的返回数据
                    Fast.api.close(data);//这里是重点
                    Toastr.success("成功");//这个可有可无
                }, function(data, ret){
                    Toastr.success("失败");
                });
            }
        }
    };
    
    return Controller;
});